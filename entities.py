__author__ = 'Sam.I'


import abc
from memory import MemoryLog
from datetime import datetime


class BasePlayer(object):
    """
    both human and computer players will inherit from this
    base class. This class will have functions such as making moves and managing who player opponents are
    """

    __metaclass__ = abc.ABCMeta

    def __init__(self, name):
        self.name = name
        # symbol is the player icon on the board
        self.symbol = ""
        # the other player
        self.opponent = None

    def set_symbol(self, symbol):
        self.symbol = symbol

    # starts the players turn
    def notify_turn(self, board):
        self.make_move(board)

    def make_move(self, board):
        #  get current available moves from the board
        available_moves = board.get_available_moves()

        # if there are no available moves.. the game should be a tie..
        if not available_moves:
            Game.get_instance().notify_tie()

        move = self.make_selection(available_moves)

        # try to make the move
        # if the move failed... recursively call the function...
        if not board.add_move(move, self.symbol, self.name):
            self.make_move(board)

    @abc.abstractmethod
    def make_selection(self, available_moves):
        """
        selects a move option from the available_moves array

        :type available_moves : list
        :param available_moves: array listing all available moves for current turn

        :rtype: str
        :return: the selected move
        """


class Computer(BasePlayer):
    def __init__(self, name):
        super().__init__(name)

    def make_selection(self, available_moves):
        """
        selects a move option from the available_moves array

        :type available_moves : list
        :param available_moves: array listing all available moves for current turn

        :rtype: str
        :return: the selected move
        """
        # get the best moves for this turn
        best_moves = self.eliminate_failed_moves(available_moves)
        # make selection at random for this turn
        selection = self.get_random(best_moves)
        return selection

    def eliminate_failed_moves(self, available_moves):
        """
        Removes moves that the computer already experienced as failed moves.
        If all moves are equally volatile, return all available moves.

        :type available_moves : list
        :param available_moves: list containing the available moves to make this turn

        :rtype: list
        :return: list containing the "best" possible moves for the computer to make
                this turn
        """

        # fetch the memory json from the game instance
        game = Game.get_instance()
        memory = game.game_history

        print("All available moves:\n{}".format(available_moves))

        # if the dictionary is empty, return all moves
        if not memory:
            return available_moves

        remaining_moves = available_moves.copy()
        # somewhat not efficient.. but... where going to iterate through all entries in the memory json
        for key in memory:
            past_game = memory[key]
            winning_symbol = past_game["winning_symbol"]
            losing_symbol = past_game["losing_symbol"]
            # the last move made by the losing player...being the move that cause them to lose
            failed_move = past_game["failed_move"]
            board_list = past_game["board_list"]

            # find the locations of the winning moves from the game entry and compare it the locations of the
            # opponent for the current game instance
            winning_symbol_locations = Computer.extract_locations(winning_symbol, board_list)
            current_opponent_locations = Computer.extract_locations(self.opponent.symbol, game.board.board_list)

            # if the two location sets match, we remove the key (k) that represents the value (val) matching the
            # failed move from the game entry
            if winning_symbol_locations == current_opponent_locations:
                for k, val in Board.MOVE_DICT.items():
                    if val == failed_move:
                        try:
                            remaining_moves.remove(k)
                        except ValueError:
                            pass

        print("Best available moves:\n{}".format(remaining_moves))
        # if there are no best moves for this turn...return all available moves
        if not remaining_moves:
            return available_moves

        # return the best moves for this turn
        return remaining_moves

    @staticmethod
    def extract_locations(symbol, board_list):
        """
        finds all the locations on the board where the passed in symbols are located

        :type symbol : str
        :param symbol:

        :type board_list : list
        :param board_list:
        """
        location_indicies = list()
        for i in range(0, len(board_list)):
            if board_list[i] == symbol:
                location_indicies.append(i)

        return location_indicies

    def get_random(self, best_moves):
        """
        chooses a random index from the passed in best moves list

        :type best_moves : list
        :param best_moves

        :rtype: str
        :return: the value stored in a randomly selected index from the best_moves list
        """
        from random import randint

        list_size = len(best_moves)
        return best_moves[randint(0, list_size - 1)]


class Human(BasePlayer):
    def __init__(self, name):
        super().__init__(name)

    def make_selection(self, available_moves):
        """
        selects a move option from the available_moves array
        Prompts user for move and takes input form stdin

        :type available_moves : list
        :param available_moves: array listing all available moves for current turn

        :rtype: str
        :return: the selected move
        """
        # prompt user for move
        return input('-Select and enter one move from the following {}'.format(available_moves))


class Game(object):
    """
    The main class for the application. This class handles starting and stopping
    the game..
    Checks if a player has won after each turn.
    """
    _game_singleton = None

    def __init__(self):
        self.board = Board()
        self.turn = ""
        self.p1 = None
        self.p2 = None
        temp_dict = MemoryLog.get_memory_json()
        self.game_history = temp_dict if type(temp_dict) is dict else dict()
        self.count = 0

    @staticmethod
    def get_instance():
        if not Game._game_singleton:
            Game._game_singleton = Game()

        return Game._game_singleton

    def set_players(self, p1, p2):
        """

        :type p1 : BasePlayer
        :type p2 : BasePlayer
        """
        self.p1 = p1
        self.p2 = p2

    def set_player_symbols(self, player):
        while True:
            p_symbol = input("Enter one character symbol for {}\n".format(player.name))
            if len(p_symbol) != 1 or p_symbol == " ":
                p_symbol = input("Invalid input...Enter one character symbol for  {}\n".format(player.name))
            else:
                player.symbol = p_symbol
                return

    def start_game(self):

        self.board.clear_board()

        # if this is the first game, we need the user to provide symbols for the players...
        if self.count == 0:
            self.set_player_symbols(self.p1)
            self.set_player_symbols(self.p2)

        # let each player know who their opponents are
        self.p1.opponent = self.p2
        self.p2.opponent = self.p1

        # increment game count
        self.count += 1
        print("Starting game {}\n".format(self.count))

        print("Begin!\n")
        self.board.print_board()

        # change whos turn it is and notify the player...
        self.turn = self.p1.symbol
        self.p1.notify_turn(self.board)
        print("Waiting for {}\n".format(self.turn))

    def notify_change(self, has_winner):
        """
        notify the game of change to the board

        :type has_winner : tuple
        """
        if has_winner[0]:
            self.declare_winner(has_winner[1])
        else:
            self.change_turn()

    def change_turn(self):
        if self.turn == self.p1.symbol:
            self.turn = self.p2.symbol
            self.p2.notify_turn(self.board)
        else:
            self.turn = self.p1.symbol
            self.p1.notify_turn(self.board)

        print("Waiting for {}\n".format(self.turn))

    def notify_tie(self):
        print("Game ends in a draw")
        self.prompt_new_game()

    def declare_winner(self, winning_symbol):
        if winning_symbol == self.p1.symbol:
            winner = self.p1
            self.save_game_history(winning_symbol, self.p2.symbol)
        else:
            winner = self.p2
            self.save_game_history(winning_symbol, self.p1.symbol)

        print("The winner is {}\n".format(winner.name))
        self.prompt_new_game()

    def prompt_new_game(self):
        self.log_history_to_file()
        again = input("Do you want to play again? y/n\n").lower()

        while True:
            if again == 'y':
                self.start_game()
                break
            elif again == 'n':
                Game.end_game()
                break
            else:
                again = input("Invalid input...Do you want to play again? y/n\n").lower()

    def save_game_history(self, winning_symbol, losing_symbol):

        history_entry = {
            "board_list": self.board.prev_board_list,
            "failed_move": self.board.prev_move,
            "losing_symbol": losing_symbol,
            "winning_symbol": winning_symbol
        }
        # get current date and time...this will be te key
        date_now = datetime.now().strftime("%m/%d/%Y %I:%M:%S %p")

        self.game_history[date_now] = history_entry

    def log_history_to_file(self):
        MemoryLog.log_memory(self.game_history)

    # exit the program
    @staticmethod
    def end_game():
        quit()


class Board:
    """
    Manages board functions such as updating the board ui with the players move.
    Board will contain a list
    """

    # dictionary that contains all possible moves for the game...
    # these moves correspond to the index in the board list array
    MOVE_DICT = {
        "A1": 0,
        "B1": 1,
        "C1": 2,
        "A2": 3,
        "B2": 4,
        "C2": 5,
        "A3": 6,
        "B3": 7,
        "C3": 8
    }

    EMPTY = " "

    def __init__(self):
        self.board_list = [Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY,
                           Board.EMPTY, Board.EMPTY]
        self.prev_board_list = list()
        self.prev_move = 0
        self.move_count = 0

    # resets the board to the initial state
    def clear_board(self):
        self.board_list = [Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY, Board.EMPTY,
                           Board.EMPTY, Board.EMPTY]
        self.prev_board_list = list()
        self.prev_move = 0
        self.move_count = 0

    def get_available_moves(self):
        avail_moves = list()

        for key, val in Board.MOVE_DICT.items():
            if self.board_list[val] == Board.EMPTY:
                avail_moves.append(key)

        avail_moves.sort()
        return avail_moves

    def add_move(self, move, symbol, player_name):
        # find the index from the move dictionary
        index = Board.MOVE_DICT[move.upper()]
        if self.has_move(index):
            print("Sorry. That move is invalid\n")
            return False
        else:
            self.move_count += 1
            # store previous board state
            self.prev_board_list = self.board_list.copy()
            # place the corresponding player symbol in the board array
            self.board_list[index] = symbol

            Board.print_move(move, player_name)
            self.print_board()

            # check the board for a winner
            has_win = self.check_for_win()
            if not has_win[0]:
                self.prev_move = index

            Game.get_instance().notify_change(has_win)
            return True

    def has_move(self, index):
        """
        checks if the move already exists on the table

        :type index: int
        :param index: integer that represents an index in the board list array

        :rtype: bool
        :return: True if move exists...False otherwise
        """
        if self.board_list[index] == Board.EMPTY:
            return False

        return True

    def check_for_win(self):
        horiz = self.win_horizontal()
        vert = self.win_vertical()
        diag = self.win_diagonal()

        if horiz[0]:
            return horiz

        if vert[0]:
            return vert

        if diag[0]:
            return diag
        return False, ""

    def win_horizontal(self):
        boxes = ["A1", "A2", "A3"]
        for box in boxes:
            # grab the index represented by the board position
            index = Board.MOVE_DICT[box]
            # create a sub array from the index position to the right end of the board
            sub_array = self.board_list[index:index+3]
            # grab the symbol in the first index of sub array
            # it doesnt matter which index we grab the symbol from because all three have to
            # match in order to have a winner
            symbol = sub_array[0]
            # check if all indices contain this symbol
            if symbol is not " " and sub_array.count(symbol) == 3:
                return True, symbol

        return False, ""

    def win_vertical(self):
        boxes = ["A1", "B1", "C1"]
        for box in boxes:
            # grab the index represented by the board position
            index = Board.MOVE_DICT[box]
            # create a sub array from the index position to the right end of the board
            sub_array = self.board_list[index::3]
            # grab the symbol in the first index of sub array
            # it doesnt matter which index we grab the symbol from because all three have to
            # match in order to have a winner
            symbol = sub_array[0]
            # check if all indices contain this symbol
            if symbol is not " " and sub_array.count(symbol) == 3:
                return True, symbol

        return False, ""

    def win_diagonal(self):
        boxes = ["A1", "C1"]

        increment = 4
        for i in range(0, 2):
            # grab the index represented by the board position
            index = Board.MOVE_DICT[boxes[i]]
            # create a sub array from the index position to the right end of the board
            sub_array = self.board_list[index::increment]
            # grab the symbol in the first index of sub array
            # it doesnt matter which index we grab the symbol from because all three have to
            # match in order to have a winner
            symbol = sub_array[0]
            # check if all indices contain this symbol
            if symbol is not " " and sub_array.count(symbol) == 3:
                return True, symbol
            increment = 2

        return False, ""

    @staticmethod
    def print_move(move, player_name):
        print("{} has made move on {}\n".format(player_name, move))

    def print_board(self):
        bl = self.board_list
        print("""
        Move {}:
               A :: B :: C
               -- -- -- --
            1| {} :: {} :: {}
            2| {} :: {} :: {}
            3| {} :: {} :: {}

        """.format(self.move_count, bl[0], bl[1], bl[2], bl[3], bl[4], bl[5], bl[6], bl[7], bl[8]))
