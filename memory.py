__author__ = 'Sam.I'

from datetime import datetime
import io
import os
import json

ENCODING = 'utf-8'
BUF_SIZE = 1024


class MemoryLog:

    @staticmethod
    def check_file_exist():
        file_size = None
        try:
            file_size = os.path.getsize("ai_memory.mem")
        except OSError:
            pass
        if file_size is None or file_size == 0:
            file = open("ai_memory.mem", "wb")
            file.write("".encode(ENCODING))
            file.flush()
            file.close()

    @staticmethod
    def log_memory(game_hist_dict):
        # dump dictionary to string
        log_entry = json.dumps(game_hist_dict, sort_keys=True)

        # open log file and write entry... this should overwrite what ever is already in the file
        file = open("ai_memory.mem", "wb")
        file.write(log_entry.encode(ENCODING))
        file.flush()
        file.close()

        # output to another file for human readability
        file = open("ai_memory_human_readable.mem", "wb")
        file.write(json.dumps(game_hist_dict, sort_keys=True, indent=4).encode(ENCODING))
        file.flush()
        file.close()

    # @staticmethod
    # def log_memory(board_list, failed_move, losing_symbol):
    #
    #     MemoryLog.check_file_exist()
    #
    #     file = open("ai_memory.mem", "rb+")
    #     # seek to second to last byte in file
    #     file.seek(-1, io.SEEK_END)
    #
    #     # get current date and time...this will be te key
    #     date_now = datetime.now().strftime("%m/%d/%Y %I:%M%p")
    #     game_json = MemoryLog.form_json(board_list, failed_move, losing_symbol)
    #
    #     log_entry = "\n\t{}: {},\n".format(date_now, json.dumps(game_json)) + "}"
    #     file.write(log_entry.encode(ENCODING))
    #     file.flush()
    #     file.close()

    @staticmethod
    def get_memory_json():
        MemoryLog.check_file_exist()

        file = open("ai_memory.mem", "rb")
        file_data = ""

        for read_chunk in MemoryLog.read_in_chunks(file):
            file_data += read_chunk.decode(ENCODING)

        file.close()
        try:
            return json.loads(file_data)
        except ValueError:
            return dict()

    @staticmethod
    def read_in_chunks(file):
        while True:
            buf = file.read(BUF_SIZE)
            if not buf:
                break
            yield buf
