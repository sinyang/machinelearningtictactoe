# README #

### What is this repository for? ###

* Console based Tic Tac Toe game where the computer learns how to play and win. Rather than
having the computer start with a set of what are winning combinations for the game, the computer
initially starts off knowing nothing about the game. After each game, the winning move is added
to the computer's memory log, which is accessed during each computer move. More work needs to be done
to improve gameplay and occasional crashes. 

### How do I get set up? ###

* **Use Python 3**.
* execute the main.py file and follow console instructions.
